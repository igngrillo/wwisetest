﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SceneInitializationModule : MonoBehaviour
{
    public abstract void Init();
    public abstract void Shutdown();
}
