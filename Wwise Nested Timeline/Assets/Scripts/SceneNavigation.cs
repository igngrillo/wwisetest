﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneNavigation : MonoBehaviour
{
    [SerializeField] private SceneNames sceneToJump = SceneNames.Story_020_Timeline;

    private void OnGUI()
    {
        DrawTimelineNavigationButtons();
    }

    private void DrawTimelineNavigationButtons()
    {
        DrawJumpToStartOfSceneButtons();
        DrawJumpToMiddleOfSceneButtons();
    }

    private void DrawJumpToStartOfSceneButtons()
    {
        if (GUI.Button(new Rect(5, 5, 200, 20), $"Move to {SceneNames.Story_010_Timeline}"))
        {
            JumpToStartOfScene(SceneNames.Story_010_Timeline);
        }
        if (GUI.Button(new Rect(5, 30, 200, 20), $"Move to {SceneNames.Story_020_Timeline}"))
        {
            JumpToStartOfScene(SceneNames.Story_020_Timeline);
        }
        if (GUI.Button(new Rect(5, 55, 200, 20), $"Move to {SceneNames.Story_030_Timeline}"))
        {
            JumpToStartOfScene(SceneNames.Story_030_Timeline);
        }
    }

    private void DrawJumpToMiddleOfSceneButtons()
    {
        string sceneToJumpName = sceneToJump.ToString();

        if (GUI.Button(new Rect(5, 90, 330, 20), $"Move to {sceneToJumpName} Offseted (Not working)"))
        {
            JumpWithOffset();
        }

        if (GUI.Button(new Rect(5, 115, 400, 20), $"Move to {sceneToJumpName} Offseted + Evaluate (Not working)"))
        {
            JumpWithOffsetAndEvaluate();
        }

        if (GUI.Button(new Rect(5, 140, 650, 20), $"Move to {sceneToJumpName} Offseted + Evaluate, Pause and Play (This only Works if on the same scene)"))
        {
            JumpWithOffsetAndCompleteEvaluation();
        }

        if (GUI.Button(new Rect(5, 165, 670, 20), $"Move to {sceneToJumpName} Offseted (This works most of times, specially when played from a different scene)"))
        {
            DoubleJumpWithEvaluation();
        }

        if (GUI.Button(new Rect(5, 190, 400, 20), $"Move to {sceneToJumpName} Offseted (This works most of times)"))
        {
            StartCoroutine(JumpToSceneSkippingFrameCoroutine());
        }
    }

    /// <summary>
    /// Calls a simple JumpToScene to the start of the scene
    /// </summary>
    /// <param name="sceneName"></param>
    private void JumpToStartOfScene(SceneNames sceneName)
    {
        MasterTimeline.Instance.JumpToScene(sceneName);
    }

    /// <summary>
    /// Calls a simple JumpToScene with an arbitrary offset
    /// </summary>
    private void JumpWithOffset()
    {
        MasterTimeline.Instance.JumpToScene(sceneToJump, 30d);
    }

    /// <summary>
    /// Calls a JumpToScene and then proceed to evaluate the timeline
    /// </summary>
    private void JumpWithOffsetAndEvaluate()
    {
        MasterTimeline.Instance.JumpToScene(sceneToJump, 30d);
        MasterTimeline.Instance.Controller.ManualEvaluate();
    }

    /// <summary>
    /// Calls a JumpToScene and then proceed to pause it, evaluate it and play it
    /// </summary>
    private void JumpWithOffsetAndCompleteEvaluation()
    {
        MasterTimeline.Instance.JumpToScene(sceneToJump, 30d);
        MasterTimeline.Instance.Controller.PlayablePause();
        MasterTimeline.Instance.Controller.ManualEvaluate();
        MasterTimeline.Instance.Controller.Play();
    }

    /// <summary>
    /// Calls two times the process of jumping to the scene and evaluating it.
    /// The first time we go to the beggining of the scene, the second time its the one that we apply the offset
    /// </summary>
    private void DoubleJumpWithEvaluation()
    {
        MasterTimeline.Instance.JumpToScene(sceneToJump);
        MasterTimeline.Instance.Controller.ManualEvaluate();

        MasterTimeline.Instance.JumpToScene(sceneToJump, 30d);
        MasterTimeline.Instance.Controller.ManualEvaluate();
    }

    /// <summary>
    /// Jumps to the start of the scene, waits for a single frame and then finishes with jumping again using the offset and evaluating the timeline
    /// </summary>
    /// <returns></returns>
    private IEnumerator JumpToSceneSkippingFrameCoroutine()
    {
        MasterTimeline.Instance.JumpToScene(sceneToJump);
        yield return null;
        MasterTimeline.Instance.JumpToScene(sceneToJump, 30d);
        MasterTimeline.Instance.Controller.ManualEvaluate();
    }
}
