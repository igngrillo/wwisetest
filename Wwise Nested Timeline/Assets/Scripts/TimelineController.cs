﻿using UnityEngine;
using UnityEngine.Playables;

public class TimelineController
{
	PlayableDirector playable;

	public double CurrentTime => playable ? playable.time : 0;

	public TimelineController(PlayableDirector playable)
	{
		if (playable == null)
			Debug.LogError($"The provided Playable for the TimelineController is null");

		this.playable = playable;
	}

	public void JumpTo(double time)
	{
		if (playable)
		{
			if (time < 0)
				time = 0;

			if (time > playable.duration)
				time = playable.duration;

			playable.time = time;
		}
	}

	public void MultiplyTime(float multiplier)
	{
		if (playable)
			JumpTo(playable.time + multiplier * Time.deltaTime);
	}

	public void Play()
	{
		if (playable)
			playable.Play();
	}

	public void Stop()
	{
		if (playable)
			playable.Stop();
	}

	public void PlayablePause()
	{
		if (playable)
			playable.Pause();
	}

	public void PlayableResume()
	{
		if (playable)
			playable.Resume();
	}

	public void Pause()
	{
		//If its not playing, it doesnt pause
		if (!playable || playable.state != PlayState.Playing)
			return;

		playable.playableGraph.GetRootPlayable(0).SetSpeed(0);
	}

	public void Resume()
	{
		//If its not playing, it doesnt resume
		if (!playable || playable.state != PlayState.Playing)
			return;

		playable.playableGraph.GetRootPlayable(0).SetSpeed(1);
	}

	public void ManualEvaluate()
	{
		if (playable)
			playable.Evaluate();
	}
}