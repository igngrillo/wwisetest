﻿using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.Timeline;
#endif
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[DisallowMultipleComponent, RequireComponent(typeof(PlayableDirector), typeof(SignalReceiver))]
public class MasterTimeline : MonoBehaviour
{

#if UNITY_EDITOR
	private const RefreshReason TIMELINE_REFRESH_REASON = RefreshReason.ContentsAddedOrRemoved;
#endif

	public static MasterTimeline Instance;

	public const string SCENE_FINISH_SIGNAL_NAME = "SceneFinish";
	public const string CHAPTER_FINISH_SIGNAL_NAME = "ChapterFinish";
	public const string LOAD_NEXT_SCENE_SIGNAL_NAME = "LoadNextScene";
	public const string AUDIO_TRACK_NAME = "Audio_Track";

	// This needs to be static, otherwise it will be null in editor
	private static string[] sceneNames;

	[SerializeField, HideInInspector] private TimelineAsset masterTimeline;

	private PlayableDirector _masterDirector;
	private SignalReceiver _signalReceiver;

	private string[] SceneNames => sceneNames != null ? sceneNames : sceneNames = Enum.GetNames(typeof(SceneNames));
	public PlayableDirector masterDirector => _masterDirector ? _masterDirector : _masterDirector = GetComponent<PlayableDirector>();
	public SignalReceiver SignalReceiver => _signalReceiver ? _signalReceiver : _signalReceiver = GetComponent<SignalReceiver>();
	public TimelineAsset MasterTimelineAsset => masterTimeline ? masterTimeline : masterTimeline = masterDirector.playableAsset as TimelineAsset;
	public TimelineController Controller { get; private set; }

	private void Awake()
	{
		Init();
	}

    private void Start()
    {
		FillTimelineReferences();
	}

    #region Public Interface
    public void Init()
	{
		if (!Instance)
		{
			Instance = this;

			DontDestroyOnLoad(this);
		}
		else DestroyImmediate(this);

		_masterDirector = GetComponent<PlayableDirector>();
		_signalReceiver = GetComponent<SignalReceiver>();

		Controller = new TimelineController(masterDirector);

		masterTimeline = masterDirector.playableAsset as TimelineAsset;
	}

	[ContextMenu("Generate")]
	public void FillTimelineReferences()
	{
		// Set the timeline if is not set
		if (masterTimeline == null)
		{
			masterTimeline = masterDirector.playableAsset as TimelineAsset;
		}

		FillSceneClips();

		masterDirector.RebuildGraph();

#if UNITY_EDITOR
		TimelineEditor.Refresh(TIMELINE_REFRESH_REASON);
#endif
	}

	public void SetMasterTimeline(TimelineAsset timeline, SignalTrack signalTrack)
	{
		// Setup the master director with the new timeline
		masterDirector.playableAsset = timeline;
		masterDirector.SetGenericBinding(signalTrack, SignalReceiver);
		masterDirector.RebuildGraph();
		// Set the new masterTimeline
		masterTimeline = masterDirector.playableAsset as TimelineAsset;
		// Refresh the timeline window if editor
#if UNITY_EDITOR
		TimelineEditor.Refresh(TIMELINE_REFRESH_REASON);
#endif
	}

	public void JumpToScene(SceneNames sceneName, double offset = 0)
	{
		var sceneTimes = SceneDataDrawer.Instance.GetSceneTimes(sceneName);

		if (sceneTimes == null) return;

		var time = sceneTimes.startTime + offset;

		Debug.Log($"Time jump to {time}");

		Controller.JumpTo(time);
	}
	#endregion Public Interface

	#region Private Methods
	private TrackAsset GetSceneTrack(string sceneName)
	{
		var tracks = masterTimeline.GetRootTracks();
		foreach (var track in tracks)
		{
			if (track.name.ToLower() == sceneName.ToLower())
			{
				return track;
			}
		}
		Debug.LogError($"No track found for scene {sceneName}.", gameObject);
		return null;
	}

	private void FillSceneClips()
	{
		var directors = GetScenesDirectors();

		foreach (var director in directors)
		{
			FillSceneTimeline(director.Key, director.Value);
		}
	}

	private void FillSceneTimeline(string sceneName, PlayableDirector playableDirector)
	{
		if (playableDirector == null)
		{
			Debug.LogError($"Playable director timeline is null for scene {sceneName}.", gameObject);
			return;
		}

		TrackAsset sceneTrack = GetSceneTrack(sceneName);

		if (sceneTrack == null)
		{
			Debug.LogError($"No track found for {sceneName}.", gameObject);
		}
		else
		{
			var clips = sceneTrack.GetClips();
			foreach (var clip in clips)
			{
				ConfigClip(clip, playableDirector.gameObject);
			}
		}
	}

	private void ConfigClip(TimelineClip clip, GameObject sourceGameObject)
	{
		var controlClip = clip.asset as ControlPlayableAsset;
		if (controlClip != null)
		{
			masterDirector.SetReferenceValue(controlClip.sourceGameObject.exposedName, sourceGameObject);
		}
	}

	private Dictionary<string, PlayableDirector> GetScenesDirectors()
	{
		var directors = new Dictionary<string, PlayableDirector>();

		var allDirectors = Resources.FindObjectsOfTypeAll<PlayableDirector>();

		for (var i = 0; i < SceneNames.Length; i++)
		{
			var sceneName = SceneNames[i];
			string[] sceneNameComponents = sceneName.Split('_');
			foreach (var sceneDirector in allDirectors)
			{
				if (IsGameObjectSceneValidAndLoaded(sceneDirector.gameObject))
				{
					TimelineAsset sceneTimeline = sceneDirector.playableAsset as TimelineAsset;
					if (sceneTimeline)
					{
						bool mustFillClip = true;

						foreach (string sceneNameComponent in sceneNameComponents)
						{
							if (!sceneTimeline.name.ToLower().Contains(sceneNameComponent.ToLower()))
							{
								mustFillClip = false;
							}
						}

						if (mustFillClip)
						{
							directors.Add(sceneName, sceneDirector);
						}
					}
				}
			}
		}

		return directors;
	}

	private bool IsGameObjectSceneValidAndLoaded(GameObject go)
	{
		return go.scene != null &&
			   go.scene.IsValid() &&
			   go.scene.isLoaded;
	}
	#endregion Private Methods
}
