﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneInitializer : MonoBehaviour
{
    [SerializeField] private List<SceneInitializationModule> modules;

    public void Init()
    {
        Debug.Log("SceneInitializer Init", gameObject);

        for (int i = 0; i < modules.Count; i++)
        {
            if (modules[i] == null)
            {
                Debug.LogError($"Null scene initialization module at index {i}");
                continue;
            }

            modules[i].Init();
        }
    }

    public void Shutdown()
    {
        Debug.Log("SceneInitializer Shutdown", gameObject);

        for (int i = 0; i < modules.Count; i++)
        {
            if (modules[i] == null)
            {
                Debug.LogError($"Null scene initialization module at index {i}");
                continue;
            }

            modules[i].Shutdown();
        }
    }
}
