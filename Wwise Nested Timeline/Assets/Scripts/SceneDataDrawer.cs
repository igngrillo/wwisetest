﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SceneDataDrawer : MonoBehaviour
{
    public static SceneDataDrawer Instance;

    public class SceneData
    {
        public SceneNames sceneName;
        public GameObject sceneRoot;
        public PlayableDirector sceneDirector;
        public PlayableDirector audioDirector;
        public SceneInitializer sceneInitializer;
        public SceneTimes sceneTimes;
    }
    public class SceneTimes
    {
        public double startTime;
        public double endTime;
    }

    private Dictionary<SceneNames, SceneData> scenesData = new Dictionary<SceneNames, SceneData>();
    private Dictionary<SceneNames, SceneTimes> scenesTimes = new Dictionary<SceneNames, SceneTimes>();

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        CacheScenesTimes();
    }

    public SceneData GetSceneData(SceneNames sceneName)
    {
        if (!scenesData.ContainsKey(sceneName))
        {
            Debug.LogError($"Trying to get an unloaded scene from the ScenesDataDrawer: {sceneName}");
            return null;
        }

        return scenesData[sceneName];
    }

    public SceneData GetCurrentSceneData()
    {
        var currentTime = MasterTimeline.Instance.Controller.CurrentTime;

        return GetSceneData(currentTime);
    }

    public SceneData GetSceneData(double currentTime)
    {
        var sceneName = GetSceneName(currentTime);

        if ((int)sceneName != -1)
        {
            if (scenesData.ContainsKey(sceneName))
            {
                return scenesData[sceneName];
            }
            else
            {
                Debug.LogError($"Scene {sceneName} is not loaded. Time {currentTime}");
                return null;
            }
        }

        return null;
    }

    public bool IsSceneDataLoaded(SceneNames sceneName)
    {
        return scenesData.ContainsKey(sceneName);
    }

    public void ClearSceneData()
    {
        scenesData.Clear();
    }

    private void OnSceneLoaded(SceneData sceneData)
    {
        if (scenesData.ContainsKey(sceneData.sceneName))
        {
            Debug.LogError($"Trying to add a scene twice to the ScenesDataDrawer: {sceneData.sceneName}");
            return;
        }

        scenesData.Add(sceneData.sceneName, sceneData);
    }

    private void OnSceneUnloaded(SceneNames sceneName)
    {
        if (!scenesData.ContainsKey(sceneName))
        {
            Debug.LogError($"Trying to remove an unloaded scene to the ScenesDataDrawer: {sceneName}");
            return;
        }

        scenesData.Remove(sceneName);
    }

    public void CacheScenesTimes()
    {
        var tracks = MasterTimeline.Instance.MasterTimelineAsset.GetOutputTracks();

        foreach (var track in tracks)
        {
            var controlTrack = track as UnityEngine.Timeline.ControlTrack;
            if (controlTrack != null && controlTrack.name.ToLower() != MasterTimeline.AUDIO_TRACK_NAME.ToLower())
            {
                var sceneName = (SceneNames)Enum.Parse(typeof(SceneNames), controlTrack.name);

                var startTime = double.MaxValue;
                var endTime = double.MinValue;

                var clips = controlTrack.GetClips();
                foreach (var clip in clips)
                {
                    if (startTime > clip.start)
                    {
                        startTime = clip.start;
                    }

                    if (endTime < clip.end)
                    {
                        endTime = clip.end;
                    }
                }

                scenesTimes.Add(sceneName, new SceneTimes()
                {
                    startTime = startTime,
                    endTime = endTime
                });
            }
        }
    }

    public SceneTimes GetSceneTimes(SceneNames sceneName)
    {
        if (!scenesTimes.ContainsKey(sceneName))
        {
            Debug.LogError($"There are no times registered for scene {sceneName}");
            return null;
        }

        return scenesTimes[sceneName];
    }

    public SceneNames GetSceneName(double time)
    {
        foreach (var sceneTime in scenesTimes)
        {
            //Debug.LogWarning($"Testing if time {time} is between {sceneTime.Value.startTime} and {sceneTime.Value.endTime} on scene {sceneTime.Key}.");
            if (time >= sceneTime.Value.startTime && time < sceneTime.Value.endTime)
            {
                return sceneTime.Key;
            }
        }

        Debug.LogError($"No scenes saved on time {time}");

        return (SceneNames)(-1);
    }
}
