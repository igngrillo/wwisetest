/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PAPERBIRDS_MUSIC_010 = 158596501U;
        static const AkUniqueID SC10_BG = 1834799902U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID BANDONEONVOLUME = 2602389325U;
        static const AkUniqueID CRYSTALVOLUME = 573230919U;
        static const AkUniqueID HANDVELOCITY = 2601967367U;
        static const AkUniqueID MUTEDIALOG = 1274160338U;
        static const AkUniqueID PORTALSCALE = 1758294411U;
        static const AkUniqueID PORTALTOSUNANGLE = 2155438739U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID DIALOG = 1235749641U;
        static const AkUniqueID GLOBAL = 1465331116U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SC_010_TOWNSCHOOL = 1094905252U;
        static const AkUniqueID SC_015_SCHOOLTRANSITION = 1645756342U;
        static const AkUniqueID SC_020_LAKE = 662944586U;
        static const AkUniqueID SC_030_TOTOHOUSE = 1910729374U;
        static const AkUniqueID SC_040_HILLDRIVE = 3655866056U;
        static const AkUniqueID SC_050_GRANDFATHERHOUSE = 358864552U;
        static const AkUniqueID SC_052_GRANDFATHERBASEMENT = 2724845993U;
        static const AkUniqueID SC_060_INVISIBLEWORLD = 2228011512U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCES = 1017660616U;
        static const AkUniqueID DIALOG = 1235749641U;
        static const AkUniqueID ENV_BUS = 3962846285U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC_2D = 1939884427U;
        static const AkUniqueID MUSIC_AMB = 2815327375U;
        static const AkUniqueID NARRATOR = 3272864290U;
        static const AkUniqueID NON_ENV_BUS = 1418599241U;
        static const AkUniqueID REVERBS = 3545700988U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID SFX_2D = 2839302323U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID CAVE = 4122393694U;
        static const AkUniqueID HOUSE = 3720479411U;
        static const AkUniqueID NARR = 948652494U;
        static const AkUniqueID SCHOOL = 3661816265U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
