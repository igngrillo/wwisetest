This project is intended to demostrate and replicate some errors we found while using wwise with nested timelines on our proyect.
To play this project correctly, make sure you have all the scenes open(Base, 010_Demo, 020_Demo, 030_Demo) and select the Base scene as the Main/Master Scene.
When the scenes are played, you will find some buttons on the screen that will help you navigate the timeline in different ways.
In most cases, the buttons that navigate to the middle of the timeline make the audio pop, click or even mute themselves.
What is even more strange is that the same button with the same code has an erratic behaviour making the use of timelines with wwise even more unreliable.
Make sure to test what happens when you go from one scene to the other or when you move from to the same scene.
You will find all the info you need on the scripts, specially the mastertimeline and the scene navigation classes.